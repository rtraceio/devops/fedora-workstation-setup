#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail
shopt -s failglob

dnfpackages=(
	"git"
	"nano"
	"wget"
	"curl"
	"btop"
	"tcpdump"
	"ca-certificates"
	"cockpit"
	"p7zip"
	"p7zip-plugins"
	"podman"
	"zsh"
	"powerline-fonts"
	"wine"
	"wine-*"
	"wireguard-tools"
	"zip"
	"unzip"
	"bind-utils"
	"NetworkManager-openvpn"
	"NetworkManager-openvpn-gnome"
	"keepassxc"
	"tilix"
	"java-latest-openjdk-devel"
	"java-latest-openjdk"
	"libreoffice-langpack-de"
	"gns3-server"
	"gns3-gui"
	"numix-icon-theme-circle"
	"gnome-tweaks"
	"torbrowser-launcher"
	"dotnet-sdk-8.0"
	"chromium"
	"xorg-x11-font-utils"
	"fontconfig"
	"ffmpeg-free"
	"libavcodec-free"
	"yt-dlp"
	"inotify-tools"
)

echo "➜ Update Machine Firmware"
fwupdmgr refresh --force > /dev/null
fwupdmgr get-updates -y > /dev/null
fwupdmgr update -y > /dev/null

echo "➜ Cleanup Firewall"
firewall-cmd --permanent --remove-port=1025-65535/udp
firewall-cmd --permanent --remove-port=1025-65535/tcp
firewall-cmd --permanent --remove-service=mdns
firewall-cmd --permanent --remove-service=ssh
firewall-cmd --permanent --remove-service=samba-client

echo "➜ Configure dnf"
grep -Fxq 'deltarpm=true' '/etc/dnf/dnf.conf' || echo 'deltarpm=true' >> '/etc/dnf/dnf.conf'
grep -Fxq 'countme=false' '/etc/dnf/dnf.conf' || echo 'countme=false' >> '/etc/dnf/dnf.conf'

echo "➜ Updating System"
dnf update -y > /dev/null

echo "➜ Installing dnf Packages"
for dnfpackage in "${dnfpackages[@]}"; do
	echo -n "Installing $dnfpackage ..."
  if (dnf install -y "$dnfpackage" > /dev/null)
    then echo -e " \e[32mOK\e[0m";
    else echo -e " \e[31mFAILED\e[0m";
  fi
done

echo "➜ Installing Media Codecs"
dnf install -y gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel > /dev/null
dnf install -y lame\* --exclude=lame-devel > /dev/null
dnf group upgrade -y --with-optional Multimedia > /dev/null

echo "➜ Enabling fstrim"
systemctl enable --now fstrim.timer > /dev/null

echo "➜ Disabling abrt"
systemctl disable abrt-journal-core abrt-oops abrt-xorg > /dev/null

echo "➜ Hardening System"
echo "    Randomizing MAC adresses..."
cp -f ./conf/00-randomize-mac.conf /etc/NetworkManager/conf.d/00-randomize-mac.conf > /dev/null
echo "    Applying sysctl Security Overrides..."
cp -f ./conf/30-security-overrides.conf /etc/sysctl.d/30-security-overrides.conf > /dev/null
echo "    Disabling automatic mounting..."
cp -f ./conf/dconf-automount-disable /etc/dconf/db/local.d/automount-disable > /dev/null
cp -f ./conf/dconf-locks-automount-disable /etc/dconf/db/local.d/locks/automount-disable > /dev/null
echo "    Unload not needed Kernel modules"
cp -f ./conf/30-kmod-blacklist.conf /etc/modprobe.d/30-blacklist.conf > /dev/null
