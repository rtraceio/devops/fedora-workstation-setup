#!/usr/bin/env bash

# git
alias gc='git commit -m'
alias gp='git push'

# top
alias top='btop'
alias htop='btop'

# fs
alias ....='cd ../../..'
alias ...='cd ../..'
alias ..='cd ..'

# tools
alias ytdl="yt-dlp"
alias youtube-dl="yt-dlp"

# convenience
alias myip="curl https://ipinfo.io/json"
alias weather="curl https://wttr.in"