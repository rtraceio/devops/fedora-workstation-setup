#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail
shopt -s failglob

echo "➜ Configuring GNOME"

gsettings set org.gnome.desktop.privacy report-technical-problems false

gsettings set org.gnome.desktop.interface clock-show-date true
gsettings set org.gnome.desktop.interface clock-show-seconds true
gsettings set org.gnome.desktop.interface show-battery-percentage true
gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
gsettings set org.gnome.desktop.sound allow-volume-above-100-percent true

gsettings set org.gnome.desktop.wm.preferences num-workspaces 5
gsettings set org.gnome.desktop.wm.preferences raise-on-click true
gsettings set org.gnome.desktop.wm.preferences resize-with-right-button false
gsettings set org.gnome.desktop.wm.preferences theme 'Adwaita'
gsettings set org.gnome.desktop.wm.preferences titlebar-font 'Cantarell Bold 10'
gsettings set org.gnome.desktop.wm.preferences titlebar-uses-system-font true
gsettings set org.gnome.desktop.wm.preferences button-layout "'appmenu:minimize,maximize,close'"

gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true
gsettings set org.gnome.settings-daemon.plugins.color night-light-temperature 1800

gsettings set org.gnome.TextEditor spellcheck true
gsettings set org.gnome.TextEditor style-scheme 'Adwaita'
gsettings set org.gnome.TextEditor style-variant 'dark'
gsettings set org.gnome.TextEditor highlight-current-line true
gsettings set org.gnome.TextEditor show-map true
gsettings set org.gnome.TextEditor show-grid true
gsettings set org.gnome.TextEditor show-line-numbers true
gsettings set org.gnome.TextEditor indent-style 'tab'
gsettings set org.gnome.TextEditor tab-width 'uint32 4'

gsettings set org.gnome.shell favorite-apps "['firefox.desktop', 'org.gnome.Nautilus.desktop', 'com.gexperts.Tilix.desktop', 'org.keepassxc.KeePassXC.desktop', 'com.visualstudio.code.desktop', 'com.jetbrains.Rider.desktop', 'com.jetbrains.PyCharm-Community.desktop', 'com.jetpackduba.Gitnuro.desktop', 'org.signal.Signal.desktop', 'im.riot.Riot.desktop']"
