# Fedora Workstation Setup

This is my small set-up (post-install) script for Fedora. It is not very likely that you can use it for your use-case.
However, you might want to use it as the foundation for your own post-install script. Just cherry-pick the parts you like.
If you need help, or have questions, feel free to contact me - I'm happy to help.

## Instructions

First, clone the repository, then change directory into the freshly cloned repository
```bash
git clone git@gitlab.com:rtraceio/devops/fedora-workstation-setup.git
cd fedora-workstation-setup
```

Then, make all scripts in the repository root directory executable.

```bash
chmod +x ./configure_gnome.sh
chmod +x ./setup_root.sh
chmod +x ./setup_user.sh
```


Finally, execute the script in the following order

```bash
./setup_root.sh
./setup_user.sh
./configure_gnome.sh
``` 