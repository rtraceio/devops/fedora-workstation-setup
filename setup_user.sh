#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail
shopt -s failglob

ENVIRONMENT_CONFIGURATION_BASEPATH="$HOME/.config/environment-profile"
ENVIRONMENT_AUTOSTART_DIRECTORY="$HOME/.config/autostart"

addPersistentEnvironmentVariable() {
	local name=$1; value=$2
	mkdir -p "$ENVIRONMENT_CONFIGURATION_BASEPATH"
	echo "#!/usr/bin/env bash" > "$ENVIRONMENT_CONFIGURATION_BASEPATH/$name.sh"
	echo "export $name=\"$value\"" >> "$ENVIRONMENT_CONFIGURATION_BASEPATH/$name.sh"

	# shellcheck source=/dev/null
	source "$ENVIRONMENT_CONFIGURATION_BASEPATH/$name.sh"
}

generateSSHKey() {
  local name=$1; url=$2

  if [ -e "$HOME/.ssh/$name" ]; then
	echo "SSH Key for $name ($url) already exists. Nothing to do."
	return
  fi

  echo "================================================================================"
  echo "Creating Public/Private Key for git ($url)"
  ssh-keygen -t ed25519 -C "Key for $name ($url) created by $USER on $HOSTNAME" -b 4096 -f "$HOME/.ssh/$name" > /dev/null
  cat ~/.ssh/"$name.pub"
  echo "================================================================================"
  read -rp "Paste this key to your account on $name. Press any Keyboard Key to continue..."
  echo ""
}

trusted_flatpaks=(
	"im.riot.Riot"
	"org.signal.Signal"
	"org.jitsi.jitsi-meet"
	"com.gitlab.newsflash"
	"com.vscodium.codium"
	"com.visualstudio.code"
	"com.jetbrains.Rider"
	"com.jetbrains.PyCharm-Community"
	"app.resp.RESP"
	"com.jetpackduba.Gitnuro"
	"rest.insomnia.Insomnia"
	"org.musescore.MuseScore"
	"io.dbeaver.DBeaverCommunity"
	"com.obsproject.Studio"
	"org.gnome.seahorse.Application"
	"org.ghidra_sre.Ghidra"
	"com.jgraph.drawio.desktop"
	"md.obsidian.Obsidian"
	"io.podman_desktop.PodmanDesktop"
	"org.kde.kdenlive"
	"re.rizin.cutter"
	"org.upscayl.Upscayl"
	"io.github.java_decompiler.jd-gui"
	"com.github.phase1geo.minder"
	"org.remmina.Remmina"
	"org.zaproxy.ZAP"
)

untrusted_flatpaks=(
	"com.discordapp.Discord"
	"com.microsoft.Teams"
	"com.valvesoftware.Steam"
)

flatpak_sdks=(
	"vala"
	"node18"
	"golang"
	"dotnet8"
	"texlive"
	"openjdk"
	"rust-stable"
)

echo "➜ Creating User Directory Structures"
mkdir -p "$HOME/.auth" > /dev/null
mkdir -p "$HOME/Workspace" > /dev/null
mkdir -p "$HOME/Playground" > /dev/null
mkdir -p "$ENVIRONMENT_AUTOSTART_DIRECTORY" > /dev/null

echo "➜ Installing Flathub Repository"
flatpak --user remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

echo "➜ Installing trusted Flatpaks"
for flatpak in "${trusted_flatpaks[@]}"; do
  echo -n "    Installing $flatpak ... "
  if(flatpak --user install -y --or-update  "$flatpak" > /dev/null)

    then echo -e " \e[32mOK\e[0m"; 
    else echo -e " \e[31mFAILED\e[0m"; 
  fi

  flatpak --user override "$flatpak" --filesystem="$HOME/Downloads" > /dev/null
  flatpak --user override "$flatpak" --filesystem="$HOME/Workspace" > /dev/null
  flatpak --user override "$flatpak" --filesystem="$HOME/Documents" > /dev/null
  flatpak --user override "$flatpak" --filesystem="$HOME/Playground" > /dev/null
done

echo "➜ Installing untrusted Flatpaks"
for flatpak in "${untrusted_flatpaks[@]}"; do
  echo -n "    Installing $flatpak ... "
  if(flatpak --user install -y --or-update "$flatpak" > /dev/null)
    then echo -e " \e[32mOK\e[0m"; 
    else echo -e " \e[31mFAILED\e[0m"; 
  fi
done

echo "➜ Installing Flatpak SDK Extensions"
for sdk in "${flatpak_sdks[@]}"; do
  echo -n "    Installing org.freedesktop.Sdk.Extension.$sdk ... "
  if(flatpak --user install -y --or-update "org.freedesktop.Sdk.Extension.$sdk")
	then echo -e " \e[32mOK\e[0m"; 
    else echo -e " \e[31mFAILED\e[0m"; 
  fi
done

echo "➜ Activate flatpak SDKs"
commaSeparatedSdks="$(echo "${flatpak_sdks[@]}" | tr ' ' ',')"
addPersistentEnvironmentVariable "FLATPAK_ENABLE_SDK_EXT" "$commaSeparatedSdks"
flatpak --user override com.jetbrains.Rider --env="FLATPAK_ENABLE_SDK_EXT=$commaSeparatedSdks" 
flatpak --user override com.visualstudio.code --env="FLATPAK_ENABLE_SDK_EXT=$commaSeparatedSdks" 

echo "➜ Configuring Autostart applications"
ln -sf "$HOME/.local/share/flatpak/exports/share/applications/im.riot.Riot.desktop" "$ENVIRONMENT_AUTOSTART_DIRECTORY"
ln -sf "$HOME/.local/share/flatpak/exports/share/applications/org.signal.Signal.desktop" "$ENVIRONMENT_AUTOSTART_DIRECTORY"
ln -sf "$HOME/.local/share/flatpak/exports/share/applications/com.discordapp.Discord.desktop" "$ENVIRONMENT_AUTOSTART_DIRECTORY"
ln -sf "$HOME/.local/share/flatpak/exports/share/applications/com.gitlab.newsflash.desktop" "$ENVIRONMENT_AUTOSTART_DIRECTORY"

echo "➜ Installing Oh My ZSH"
RUNZSH=no sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

echo "➜ Configuring environment"
sourceLoopBashLine="for f in $ENVIRONMENT_CONFIGURATION_BASEPATH/*.sh; do source \$f; done"
grep -Fxq "$sourceLoopBashLine" "$HOME/.bashrc" || echo "$sourceLoopBashLine" >> "$HOME/.bashrc"
grep -Fxq "$sourceLoopBashLine" "$HOME/.zshrc" || echo "$sourceLoopBashLine" >> "$HOME/.zshrc"

echo "➜ Configuring aliases"
# needs to be after oh my zsh installation as omzsh overwrites zsh
cp -f ./conf/shell-aliases.sh "$HOME/.shell-aliases.sh"
sourceAliasesLine="source $HOME/.shell-aliases.sh"
grep -Fxq "$sourceAliasesLine" "$HOME/.bashrc" || echo "$sourceAliasesLine" >> "$HOME/.bashrc"
grep -Fxq "$sourceAliasesLine" "$HOME/.zshrc" || echo "$sourceAliasesLine" >> "$HOME/.zshrc"

echo "➜ Configuring SSH Keys & git"
generateSSHKey "gitlab" "https://gitlab.com"
generateSSHKey "github" "https://github.com"
generateSSHKey "framagit" "https://framagit.org"
generateSSHKey "codeberg" "https://codeberg.org"
generateSSHKey "bitbucket" "https://bitbucket.org"

git config --global user.name "Raffael Rehberger"
git config --global user.email "raffael@rtrace.io"
git config --global user.signingkey "040C4B5408A096E7"
git config --global init.defaultBranch "main"
git config --global core.editor "nano"
git config --global commit.gpgSign true
git config --global tag.gpgSign true
